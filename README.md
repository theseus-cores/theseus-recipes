# Theseus PyBOMBS Recipes

## List of recipes

| Name                    | Type   | Description
|-------------------------|--------|--------------------------------------------------------------------------
| theseus-rfnoc-UHD-3.14  | prefix | UHD-3.14: RFNoC configuration, includes uhd, gr-ettus, theseus-cores


## Prerequisites

To use these recipes, you need PyBOMBS installed and initialize this recipe
repository. You also need at least the gr-recipes repository enabled.

To install the latest official release of PyBOMBS, simply run

    $ [sudo] pip install PyBOMBS

However, you might want to run the latest bleeding edge version of PyBOMBS,
which you can install by running

    $ [sudo] pip install [--upgrade] git+https://github.com/gnuradio/pybombs.git

After initialization, make sure you have the gr-recipes and ettus-pybombs
repositories enabled:

    $ pybombs recipes add gr-recipes git+https://github.com/gnuradio/gr-recipes.git
    $ pybombs recipes add ettus-pybombs git+https://github.com/EttusResearch/ettus-pybombs.git
    $ pybombs recipes add theseus-recipes git+https://gitlab.com/theseus-cores/theseus-recipes.git

See the PyBOMBS manual and RFNOC instructions for more details:
- https://github.com/gnuradio/pybombs/blob/master/README.md
- https://kb.ettus.com/Getting_Started_with_RFNoC_Development#Creating_a_development_environment

## Using these recipes

**Build the UHD-3.14 tag:**

    $ pybombs prefix init <folder-name> -R theseus-rfnoc-UHD-3.14
